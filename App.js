import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import TasksListScreen from './src/components/TasksListScreen'
import TasksDetailScreen from './src/components/TasksDetailScreen'
import TasksEditScreen from './src/components/TasksEditScreen'

const stack = createStackNavigator({
  Tareas: TasksListScreen,
  Detalle: TasksDetailScreen,
  Editar: TasksEditScreen,
})

export default createAppContainer(stack)
