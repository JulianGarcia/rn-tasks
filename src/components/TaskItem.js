import React, { Component } from 'react'
import { Text, View, Alert } from 'react-native'
import Checkbox from '@react-native-community/checkbox'
import { Styles, checkboxStyle } from './Styles'
import { Icon } from 'react-native-elements'

export class TaskItem extends Component {

  onValueChange( completed ){
    this.props.onTaskStateChange(completed)
  }

  render() {

    return(
      <View style={Styles.taskItem}>
        <View style={{flex:1, flexDirection:'row', justifyContent: 'flex-start'}}>
          <Checkbox 
            value={this.props.tarea.completada} 
            tintColors={{ true: checkboxStyle.tintColorsTrue, false: checkboxStyle.tintColorsFalse }}
            onValueChange={completed => this.onValueChange(completed)}
          />
          <Text style={Styles.textListItem}>{ this.props.tarea.titulo }</Text>
        </View>
        <View style={{flex:1, flexDirection: 'row', justifyContent: 'flex-end'}}>
          <Icon name='close' color='#ccc' size={24}/>
        </View>
      </View>
    )
  }
}
